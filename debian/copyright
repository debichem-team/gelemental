Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gElemental
Upstream-Contact: Graham Inggs <ginggs@riseup.net>
Source: https://github.com/ginggs/gelemental

Files: *
Copyright: 2016-2023 Graham Inggs <ginggs@riseup.net>
           2006-2007 Kevin Daughtridge <kevin@kdau.com>
           2002-2004 Ole Laursen <olau@hardworking.dk>
           2003 Jonas Frantz <jonas.frantz@welho.com>
License: GPL-3.0+

Files: libelemental/data.cc
Copyright: 2006 The Blue Obelisk Project (http://www.blueobelisk.org/)
License: Expat

Files: debian/*
Copyright: 2007 Cesare Tirabassi <norsetto@ubuntu.com>
           2008-2023 The Debichem Team <debichem-devel@lists.alioth.debian.org>
License: GPL-3.0+

License: GPL-3.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
